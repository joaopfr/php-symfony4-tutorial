<?php

namespace App\Controller;

use App\Entity\Produto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
    /**
     * @return Response
     *
     * @Route("hello-world")
     */
    public function helloWorld()
    {
        return new Response(
            "<html><body><h1>Hello World!!</h1></body></html>"
        );
    }

    /**
     * @return Response
     *
     * @Route("show-message")
     */
    public function message()
    {
        return $this->render(
            "hello/message.html.twig",
            [
                "message" => "Olá School of Net"
            ]
        );
    }

    /**
     * @return Response
     *
     * @Route("cadastrar-produto")
     */
    public function produto()
    {
        $em = $this->getDoctrine()->getManager();
        $produto = new Produto();
        $produto->setNome("Smartphone")
                ->setPreco(2000.0);

        $em->persist($produto);
        $em->flush();

        return new Response(
            "<html><body><p>O produto {$produto->getId()} foi criado.</p></body></html>"
        );
    }

    /**
     * @return Response
     *
     * @Route("formulario")
     */

    public function formulario(Request $request)
    {
        $produto = new Produto();
        $form = $this->createFormBuilder($produto)
                     ->add("nome", TextType::class)
                     ->add("preco", TextType::class)
                     ->add("enviar", SubmitType::class, ["label" => "Salvar"])
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            return new Response("Formulário está ok");
        }

        return $this->render(
            "hello/formulario.html.twig",
            ["form" => $form->createView()]
        );
    }

}