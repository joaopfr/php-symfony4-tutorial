<?php

namespace App\Controller;

use App\Entity\Produto;
use App\Form\ProdutoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProdutoController extends AbstractController
{
    /**
     * @Route("/produto", name="listar_produto")
     * @Template("produto/index.html.twig")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $produtos = $em->getRepository(Produto::class)->findAll();

        return [
            "produtos" => $produtos,
        ];
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @Route("/produto/cadastrar", name="cadastrar-produto")
     * @Template("produto/create.html.twig")
     */
    public function create(Request $request)
    {
        $produto = new Produto();
        $form = $this->createForm(ProdutoType::class, $produto);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produto);
            $em->flush();

            $this->get("session")->getFlashBag()->set("success", "Produto cadastrado com sucesso!");
            return $this->redirectToRoute("listar_produto");
        }
        return ["form" => $form->createView()];
    }


    /**
     * @param Produto $produto
     * @param Request $request
     * @return Response
     *
     * @Route("/produto/editar/{id}", name="editar-produto")
     * @Template("produto/update.html.twig")
     */
    public function update(Produto $produto, Request $request)
    {
        $form = $this->createForm(ProdutoType::class, $produto);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produto);
            $em->flush();

            $this->get("session")->getFlashBag()->set(
                "success", "O Produto {$produto->getNome()} foi alterado com sucesso"
            );
            $this->redirectToRoute("listar-produto");
        }

        return [
            "produto" => $produto,
            "form" => $form->createView()
        ];
    }

    /**
     * @param int $id
     * @return Response
     *
     * @Route("/produto/visualizar/{id}", name="visualizar-produto")
     * @Template("produto/view.html.twig")
     */
    public function view(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $produto = $em->getRepository(Produto::class)->find($id);

        return [
           "produto" => $produto
        ];
    }
}
